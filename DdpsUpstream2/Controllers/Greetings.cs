﻿using Microsoft.AspNetCore.Mvc;

namespace DdpsUpstream2.Controllers;

[ApiController]
[Route("/hello")]
public class GreetingsController : ControllerBase
{
    private readonly ILogger<GreetingsController> _logger;

    public GreetingsController(ILogger<GreetingsController> logger)
    {
        _logger = logger;
    }

    [HttpGet(Name = "SayHello")]
    public string Get()
    {
        return "Hello, DDPS Series reader!";
    }
}
